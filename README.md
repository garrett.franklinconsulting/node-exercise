# node-exercise
A little exercise using a Star Wars API [https://swapi.dev/](https://swapi.dev/) and [express.js](https://expressjs.com/)

## Instructions To Run
I've written three npm scripts for executing:
1. `npm run start` This script will start the express server on port 3000.
2. `npm run dev` This script will start the express server using nodemon on port 3000 for debug output.
3. `npm run test` This script will run the automated tests written with Jest.
