const request = require('supertest');
const app = require('../app');
const axios = require('axios');
const swapiUrl = 'https://swapi.dev/api';

let people;
let response;
beforeAll(async () => {
  people = (await axios.get(`${swapiUrl}/people`)).data;
  response = await request(app)
    .get('/people');
}, 20000);

describe('GET /people', () => {
  it('Response should have a status code of 200.', () => {
    expect(response.status).toBe(200);
  });

  it('Response should include all people.', () => {
    const peopleCount = people.count;
    expect(response.body.length).toBe(peopleCount);
  });

  it('Response should be sorted by name when the sortBy=name query parameter is present.', async () => {
    const sortedResponse = (await request(app)
      .get('/people?sortBy=name')).body;

    let previousName = sortedResponse[0].name;
    const sorted = sortedResponse.every(({ name }) => {
      const inOrder = name >= previousName;
      previousName = name;
      return inOrder;
    });
    expect(sorted).toBe(true);
  }, 30000);

  it('Response should be sorted by mass when the sortBy=mass query parameter is present.', async () => {
    const sortedResponse = (await request(app)
      .get('/people?sortBy=mass')).body;

    let previousMass = parseFloat(sortedResponse[0].mass) || 0;
    const sorted = sortedResponse.every(({ mass }) => {
      mass = parseFloat(mass.replace(/,/g, '')) || 0;
      const inOrder = mass >= previousMass;
      previousMass = mass;
      return inOrder;
    });
    expect(sorted).toBe(true);
  }, 30000);

  it('Response should be sorted by height when the sortBy=height query parameter is present.', async () => {
    const sortedResponse = (await request(app)
      .get('/people?sortBy=height')).body;

    let previousHeight = parseInt(sortedResponse[0].height) || 0;
    const sorted = sortedResponse.every(({ height }) => {
      height = parseInt(height) || 0;
      const inOrder = height >= previousHeight;
      previousHeight = height;
      return inOrder;
    });
    expect(sorted).toBe(true);
  }, 30000);
});