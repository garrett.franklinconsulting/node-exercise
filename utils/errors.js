class GeneralError extends Error {
  constructor(message, description, exception) {
    super();
    this.message = message;
    this.description = description;
    if (exception) this.exception = exception.toString();
  }

  getCode() {
    return {code: 500, status: 'GENERAL_ERROR'};
  }
}

class FailedRequest extends GeneralError {
  getCode() {
    return {code: 500, status: 'FAILED_REQUEST'};
  }
}

class InvalidParams extends GeneralError {
  getCode() {
    return {code: 400, status: 'INVALID_PARAMS'};
  }
}

module.exports = {
  GeneralError,
  FailedRequest,
  InvalidParams
};
