const request = require('supertest');
const app = require('../app');
const axios = require('axios');
const swapiUrl = 'https://swapi.dev/api';

let planets;
let response;
beforeAll(async () => {
  planets = (await axios.get(`${swapiUrl}/planets`)).data;
  response = await request(app)
    .get('/planets');
}, 30000);

describe('GET /planets', () => {
  it('Response should have a status code of 200.', () => {
    expect(response.status).toBe(200);
  });

  it('Response should include all planets.', () => {
    const planetCount = planets.count;
    expect(response.body.length).toBe(planetCount);
  });

  it('Each planet should have the correct number of residents.', () => {
    const compareByName = (a, b) => a.name > b.name ? 1 : -1;
    const sortedPlanets = planets.results.sort(compareByName);
    const sortedResponse = response.body.sort(compareByName);

    for (const [index, planet] of sortedPlanets.entries()) {
      expect(sortedResponse[index].length).toBe(planet.length);
    }
  });
});