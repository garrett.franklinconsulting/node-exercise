/*
Typically I would create a file/router for each entity (planets, people), as well as a controller for each. Because
the solution ended up being so short to this problem, I opted to keep the routes and handlers in this file for
readability and to save on unnecessary complexity.
 */
const express = require('express');
const axios = require('axios');
const handleErrors = require('./middleware/handleErrors');
const { FailedRequest, InvalidParams } = require('./utils/errors');
const app = express();

const swapiUrl = 'https://swapi.dev/api';
app.get('/people', async (req, res, next) => {
  let people = [];

  let nextPageUrl = `${swapiUrl}/people`;
  while (nextPageUrl) {
    try {
      const swapiRes = (await axios.get(nextPageUrl)).data;
      people.push(...swapiRes.results);
      nextPageUrl = swapiRes.next;
    } catch (e) {
      next(new FailedRequest('An error occurred while retrieving people from swapi.dev.', null, e));
      return;
    }
  }

  const { sortBy } = req.query;
  if (sortBy) {
    const comparators = {
      name(a, b) {
        return a.name > b.name ? 1 : -1;
      },
      height(a, b) {
        a = parseInt(a.height) || 0;
        b = parseInt(b.height) || 0;
        return a > b ? 1 : -1;
      },
      mass(a, b) {
        a = parseFloat(a.mass.replace(/,/g, '')) || 0;
        b = parseFloat(b.mass.replace(/,/g, '')) || 0;
        return a > b ? 1 : -1;
      }
    };

    if (!Object.keys(comparators).includes(sortBy)) {
      next(new InvalidParams('Invalid "sortBy" query parameter.', 'Sort must be "name", "height", or "mass".', null));
      return;
    }
    people.sort(comparators[sortBy])
  }

  res.send(people);
});

app.get('/planets', async (req, res, next) => {
  let planets = [];
  let pagePromises = [];

  let nextPageUrl = `${swapiUrl}/planets`;
  while (nextPageUrl) {
    try {
      const swapiRes = (await axios.get(nextPageUrl)).data;
      nextPageUrl = swapiRes.next;
      // Process each page's planets in a promise so the next page of planets can be requested immediately
      const pagePromise = Promise.all(
        swapiRes.results.map(planet => {
          const residentUrls = planet.residents;
          planet.residents = []; // Set residents to an empty array that will be added to as the resident requests return
          planets.push(planet);
          return Promise.all( // Process each planet's residents in a promise so the next planet can process immediately
            residentUrls.map(residentUrl => axios.get(residentUrl)
              .then(({ data: resident }) => { planet.residents.push(resident.name) }))
          );
        })
      );

      pagePromises.push(pagePromise);
    } catch (e) {
      next(new FailedRequest('An error occurred while retrieving planets from swapi.dev.', null, e));
      return;
    }
  }

  try {
    await Promise.all(pagePromises);
  } catch (e) {
    next(new FailedRequest('An error occurred while retrieving recipient data from swapi.dev.', null, e));
    return;
  }
  res.send(planets);
});

app.use(handleErrors); // Error handling middleware
module.exports = app; // app is exported in order to use it in unit tests